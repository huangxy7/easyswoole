<?php


namespace App\HttpController;

use App\Utility\MyRedisQueue;
use EasySwoole\AtomicLimit\AtomicLimit;//限流器
use EasySwoole\Component\Di;
use EasySwoole\Http\AbstractInterface\Controller;
use EasySwoole\Queue\Exception\Exception;

class Index extends Controller
{

    /** @var AtomicLimit $autoLimiter */  
    private $autoLimiter;

    protected function onRequest(?string $action): ?bool
    {
        $this->autoLimiter = Di::getInstance()->get('auto_limiter');

        if ($action == 'index') {
            # 调用限流器对 http://127.0.0.1:9501/test1 请求限制流量
            if ($this->autoLimiter->access($action, 1)) {
                return true;
            } else {
                $this->writeJson(200, null, 'index refuse!');
                return false;
            }
        } else if ($action == 'test') {
            # 调用限流器对 http://127.0.0.1:9501/test2 请求限制流量
            if ($this->autoLimiter->access($action, 2)) {
                return true;
            } else {
                $this->writeJson(200, null, 'test refuse!');
                return false;
            }
        }

        return parent::onRequest($action);
    }

        
    public function index()
    {
        $file = EASYSWOOLE_ROOT.'/vendor/easyswoole/easyswoole/src/Resource/Http/welcome.html';
        if(!is_file($file)){
            $file = EASYSWOOLE_ROOT.'/src/Resource/Http/welcome.html';
        }
        $this->response()->write(file_get_contents($file));
    }

    function test()
    {
        $this->response()->write('this is test');
    }

    protected function actionNotFound(?string $action)
    {
        $this->response()->withStatus(404);
        $file = EASYSWOOLE_ROOT.'/vendor/easyswoole/easyswoole/src/Resource/Http/404.html';
        if(!is_file($file)){
            $file = EASYSWOOLE_ROOT.'/src/Resource/Http/404.html';
        }
        $this->response()->write(file_get_contents($file));
    }

    // 生产普通任务

    /**
     * @throws Exception
     */
    public function producer(): void
    {
        // 获取队列
        $queue = MyRedisQueue::getInstance();
        // 创建任务
        $job = new \EasySwoole\Queue\Job();
        // 设置任务数据
        $job->setJobData("生产普通任务 time " . date('Ymd h:i:s'));
        // 生产普通任务
        $produceRes = $queue->producer('topic')->push($job);
        if (!$produceRes) {
            $this->writeJson(\EasySwoole\Http\Message\Status::CODE_OK, [], '队列生产普通任务失败!');
        } else {
            $this->writeJson(\EasySwoole\Http\Message\Status::CODE_OK, [], '队列生产普通任务成功!');
        }
    }

    // 生产延迟任务

    /**
     * @throws Exception
     */
    public function producerDelay(): void
    {
        // 获取队列
        $queue = MyRedisQueue::getInstance();
        // 创建任务
        $job = new \EasySwoole\Queue\Job();
        // 设置任务数据
        $job->setJobData("生产延迟任务 time " . date('Ymd h:i:s'));
        // 设置任务延后执行时间
        $job->setDelayTime(5);
        // 生产延迟任务
        $produceRes = $queue->producer('topic')->push($job);
        if (!$produceRes) {
            $this->writeJson(\EasySwoole\Http\Message\Status::CODE_OK, [], '队列生产延迟任务失败!');
        } else {
            $this->writeJson(\EasySwoole\Http\Message\Status::CODE_OK, [], '队列生产延迟任务成功!');
        }
    }

    // 生产可信任务

    /**
     * @throws Exception
     */
    public function producerCredible(): void
    {
        // 获取队列
        $queue = MyRedisQueue::getInstance();
        // 创建任务
        $job = new \EasySwoole\Queue\Job();
        // 设置任务数据
        $job->setJobData("生产可信任务 time " . date('Ymd h:i:s'));

        // 设置任务重试次数为 3 次。任务如果没有确认，则会执行三次
        $job->setRetryTimes(3);
        // 如果5秒内没确认任务，会重新回到队列。默认为3秒
        $job->setWaitConfirmTime(5);
        // 投递任务
        $produceRes = $queue->producer('topic')->push($job);
        if (!$produceRes) {
            $this->writeJson(\EasySwoole\Http\Message\Status::CODE_OK, [], '队列生产可信任务失败!');
        } else {
            $this->writeJson(\EasySwoole\Http\Message\Status::CODE_OK, [], '队列生产可信任务成功!');
        }
    }

}