<?php

namespace App\Crontab;


use EasySwoole\Crontab\JobInterface;

class TestCrontab implements JobInterface
{

    public function jobName(): string
    {
        // 定时任务的名称
        return 'TestCrontab';
    }

    public function crontabRule(): string
    {
        // 定义执行规则 根据 Crontab 来定义
        // 这里是每五分钟执行一次
        return '*/5 * * * *';
    }

    public function run()
    {
        // 定时任务的执行逻辑

        // 相当于每分钟打印1次时间戳，这里只是参考示例。
        \EasySwoole\EasySwoole\Logger::getInstance()->info('定时任务' . $this->jobName() . '执行');
    }

    public function onException(\Throwable $throwable)
    {
        // 捕获 run 方法内所抛出的异常 ,拦截错误进日志,使控制器继续运行
        \EasySwoole\EasySwoole\Trigger::getInstance()->throwable($throwable);
    }
}
