## Crontab表达式

通用表达式：

    *    *    *    *    *
    -    -    -    -    -
    |    |    |    |    |
    |    |    |    |    |
    |    |    |    |    +----- day of week (0 - 7) (Sunday=0 or 7)
    |    |    |    +---------- month (1 - 12)
    |    |    +--------------- day of month (1 - 31)
    |    +-------------------- hour (0 - 23)
    +------------------------- min (0 - 59)
特殊表达式：

| 特殊表达式 | 特殊表达式  |
|--|--|
|@yearly    |每年一次 等同于(0 0 1 1 *)   |
| @annually   | 每年一次 等同于(0 0 1 1 *) |
| @monthly    | 每月一次 等同于(0 0 1 * *)  |           
| @weekly     | 每周一次 等同于(0 0 * * 0)  |      
| @daily         | 每日一次 等同于(0 0 * * *)  |      
| @hourly      | 每小时一次 等同于(0 * * * *)  |      
         
                   
                   
                 

## Crontab管理

EasySwoole内置对于Crontab的命令行操作，方便开发者友好的去管理Crontab。

可执行php easyswoole crontab -h来查看具体操作。

 - 查看所有注册的Crontab

**php easyswoole crontab show**

 - 停止指定的Crontab

**php easyswoole crontab stop --name=TASK_NAME**

 - 恢复指定的Crontab

**php easyswoole crontab resume --name=TASK_NAME**

 - 立即跑一次指定的Crontab

**php easyswoole crontab run --name=TASK_NAME**