<?php
namespace App\Utility;

use EasySwoole\Component\Singleton;
use EasySwoole\Queue\Queue;
class MyRedisQueue extends Queue
{
    use Singleton;
}