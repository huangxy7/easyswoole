<?php

namespace App\Process;

use App\Utility\MyRedisQueue;
use EasySwoole\Component\Process\AbstractProcess;
use EasySwoole\Queue\Exception\Exception;

//定义队列消费进程
class QueueProcess extends AbstractProcess
{
    /**
     * @throws \Throwable
     * @throws Exception
     */
    public function run($arg)
    {
        MyRedisQueue::getInstance()->consumer('topic')->listen(function ($job) {
            var_dump('消费任务:' . $job->getJobData());
            //确认一个任务(生产可信任务时使用)
            MyRedisQueue::getInstance()->consumer('topic')->confirm($job);
        });
    }
}