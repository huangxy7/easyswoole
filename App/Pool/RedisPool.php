<?php
/**
 * Created by PhpStorm.
 * User: Tioncico
 * Date: 2019/10/15 0015
 * Time: 14:46
 */

namespace App\Pool;

use EasySwoole\Pool\Config;
use EasySwoole\Pool\AbstractPool;
use EasySwoole\Redis\Redis;

class RedisPool extends AbstractPool
{
    protected  $redisConfig;


    public function __construct(Config $conf, $redisConfig)
    {
        parent::__construct($conf);
        $this->redisConfig = $redisConfig;
    }

    protected function createObject(): Redis
    {
        //根据传入的redis配置进行new 一个redis
        return new Redis($this->redisConfig);
    }
}