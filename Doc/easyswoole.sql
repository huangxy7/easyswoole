/*
 Navicat Premium Data Transfer

 Source Server         : docker_ds_mysql8
 Source Server Type    : MySQL
 Source Server Version : 50741
 Source Host           : 127.0.0.1:33068
 Source Schema         : easyswoole

 Target Server Type    : MySQL
 Target Server Version : 50741
 File Encoding         : 65001

 Date: 02/07/2024 09:43:02
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for system_config
-- ----------------------------
DROP TABLE IF EXISTS `system_config`;
CREATE TABLE `system_config` (
  `id` int(10) NOT NULL,
  `name` varchar(50) NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

SET FOREIGN_KEY_CHECKS = 1;
