# EASYSWOOLE DEMO


## 请先认真阅读手册 再进行体验

- [EASYSWOOLE在线手册](https://www.easyswoole.com)
- 开发基础版本
- redis服务
- MySQL连接处理
- Ip访问次数统计拉黑限制
- crontab 定时任务
- 敏感词过滤
- 消息队列功能
- 数据库连接预热
- 后续功能添加中。。。。

## docker-compose部署
```bash
git clone https://gitee.com/huangxy7/easyswoole.git
cd easyswoole
docker-compose up -d
```
## 启动 EASYSWOOLE
```bash
 进入docker容器
    docker exec -it php bash
 启动easyswoole
    php easyswoole server start
```
## 访问 EASYSWOOLE
```bash
    http://127.0.0.1:8072
```

